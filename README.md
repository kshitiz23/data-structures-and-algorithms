# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contains different data structures' implementation in Java

Visit - https://theheapspace.blogspot.com/

### How do I get to run the programs? ###

* Do you have Java on your machine. If yes, that's all you need! 
* If no, then install JDK according to your system specifications.
* Don't want to install Java? Just here to run the program ? Don't worry then. Just search for any online Java compiler on Google.

### Who do I talk to? ###

* Mail me at - kshitizkumar1995@gmail.com 
* Please let me know about any further optimizations or any errors. We can then work together to improve the code.